FROM ubuntu:16.04

RUN apt-get update && apt-get install -y vsftpd redis-tools jq
RUN sed -i "/start-stop-daemon --start/a sleep 1" /etc/init.d/vsftpd
RUN sed -i "s/listen=NO/listen=YES/g" /etc/vsftpd.conf
RUN sed -i "s/listen_ipv6=YES/listen_ipv6=NO/g" /etc/vsftpd.conf
RUN echo "chroot_local_user=YES\n" >> /etc/vsftpd.conf
RUN echo "allow_writeable_chroot=YES\n" >> /etc/vsftpd.conf
RUN echo "write_enable=YES\n" >> /etc/vsftpd.conf
RUN echo "download_enable=NO\n" >> /etc/vsftpd.conf
RUN echo "file_open_mode=0755\n" >> /etc/vsftpd.conf
RUN echo "local_umask=000\n" >> /etc/vsftpd.conf
RUN echo "pasv_max_port=3020\npasv_min_port=3000\n" >> /etc/vsftpd.conf

COPY run.sh /run.sh

CMD /run.sh
