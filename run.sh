echo "pasv_address=$INTERNAL_IP_ADDRESS\n" >> /etc/vsftpd.conf

service vsftpd start

lastSave=""
while true
do
  latestLastSave=`redis-cli -h redis get ftp-users`

  if [ "$lastSave" != "$latestLastSave" ]
  then
    echo `date`
    echo "Redis has been updated since last check (for first iteration)"
    lastSave=$latestLastSave

    hasUser=false
    for row in $(echo $lastSave | jq -r '.[] | @base64'); do
      _jq() {
        echo ${row} | base64 --decode | jq -r ${1}
      }
      username=$(_jq '.username')
      password=$(_jq '.password')

      if [ -z "$password" ]
      then
        userdel $username
      else
        hasUser=true
        adduser $username --disabled-login --gecos ""
        echo $username:$password | chpasswd
        usermod -d /ftp-dir $username
	# this breaks multi-user support but since we only need 1 user should be fine
	chown $username /ftp-dir
      fi
    done

    if [ "$hasUser" = "true" ]; then
      service vsftpd restart
    else
      service vsftpd stop
    fi
  fi

  sleep 5
done
